import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      router, localVue
    })
  })

  test('should increment videoIndex when next button is clicked', () => {
    expect(wrapper.vm.videoIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.videoIndex).toBe(1)
  })

  test('should navigate to story correctly when button is clicked', () => {
    const $route = {
      path: '/stanza'
    }

    wrapper.setData({
      videoIndex: wrapper.vm.videos.length - 1
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
