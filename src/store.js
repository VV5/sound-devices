import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    stanza:
      {
        id: 1,
        name: 'Stanza 1',
        main: 'Here, in the night, tree<span class="is-sibilance">s</span> <span class="is-sibilance">si</span>nk <span class="is-alliteration">d</span>eeply <span class="is-alliteration">d</span>ownward. <br> The <span class="is-imagery">s<span class="is-assonance">ou</span>nd of m<span class="is-assonance">oo</span>nlight</span> walking on black gra<span class="is-sibilance">ss</span> <br> magnifie<span class="is-sibilance">s</span> the <span class="is-alliteration">c</span>lear hard <span class="is-imagery"><span class="is-alliteration">c</span>all<span class="is-sibilance">s</span></span> of a nightjar, <br> its <span class="is-imagery"> <span class="is-alliteration">s</span><span class="is-assonance">oli</span><span class="is-assonance">lo</span>quy</span> <span class="is-assonance">of</span> <span class="is-assonance">or</span>dered <span class="is-alliteration">s</span>avagery, little interval<span class="is-sibilance">s</span>. <br> <span class="is-alliteration">T</span>ime, clinging on the wri<span class="is-sibilance">st</span>, <span class="is-onomatopoeia"><span class="is-alliteration">t</span>ick<span class="is-sibilance">s</span></span> it by <br> but eye<span class="is-sibilance">s</span>, glued to the dark pages of night, <br> could not <span class="is-alliteration">s</span>can the <span class="is-alliteration">s</span>ource on the branch. <br> <br> its in<span class="is-sibilance">si</span><span class="is-sibilance">st</span>ent cal<span class="is-sibilance">ls</span> <span class="is-onomatopoeia">jab & jab</span> so many time<span class="is-sibilance">s</span> <br> to a <span class="is-sibilance">si</span>lent ictu<span class="is-sibilance">s</span>, <span class="is-sibilance">s</span>o many time<span class="is-sibilance">s</span>, <span class="is-onomatopoeia">ringing</span> off the branch <br> in tiny <span class="is-sibilance">sh</span>arp <span class="is-onomatopoeia">tuks</span>, each lifting from the last <br> <br> through the night. while the <span class="is-sibilance">sh</span>adow<span class="is-sibilance">s</span> of the tree<span class="is-sibilance">s</span> <br> go past the edge of <span class="is-alliteration">sl</span>eep & i <span class="is-alliteration">si</span>t awake, <br> if it’s <span class="is-alliteration">f</span>oot<span class="is-alliteration">f</span>alls acro<span class="is-sibilance">ss</span> the road, they <span class="is-sibilance">sh</span>ould be <br> far away, <span class="is-sibilance">s</span>ounding on the tree<span class="is-sibilance">s</span>, an euphony <br> <span class="is-assonance">lo</span>dged <span class="is-assonance">o</span>n <span class="is-alliteration">h</span>igh, the <span class="is-alliteration">s</span>tarlit <span class="is-alliteration">s</span>ide of <span class="is-alliteration">h</span>eaven.'
      },
    sounds: [
      {
        name: 'Alliteration',
        theme: {
          color: '#fff',
          backgroundColor: '#0aad9c',
          textTransform: 'uppercase'
        },
        selected: false,
        text: 'It is when the first sound or letter for each word in a series of words is the same'
      },
      {
        name: 'Sibilance',
        theme: {
          color: '#fff',
          backgroundColor: '#ec9200',
          textTransform: 'uppercase'
        },
        selected: false,
        text: 'The deliberate repetition of soft consonants sounds such as \'s\', and \'f\' in a group of closely connected words'
      },
      {
        name: 'Assonance',
        theme: {
          color: '#fff',
          backgroundColor: '#b6064f',
          textTransform: 'uppercase'
        },
        selected: false,
        text: 'A repetition of vowel sounds within words or syllabus'
      },
      {
        name: 'Onomatopoeia',
        theme: {
          color: '#fff',
          backgroundColor: '#dfd446',
          textTransform: 'uppercase'
        },
        selected: false,
        text: 'A word that imitates the sounds it represents'
      },
      {
        name: 'Imagery',
        theme: {
          color: '#fff',
          backgroundColor: '#55cb81',
          textTransform: 'uppercase'
        },
        selected: false,
        text: 'This is not a sound device. However, you might have noticed that there is a lot of imagery related to sound in the poem. How do they contribute to the overall effect of the poem? \n Note: Imagery is a literary device used to create a picture in the reader\'s mind by making the reader see, hear, taste, smell or touch what is being described.'
      }
    ],
    correctScreen: false
  },
  mutations: {
    setSoundSelected (state, sound) {
      const index = state.sounds.indexOf(sound)
      state.sounds[index].selected = true
    },
    correctScreen (state, status) {
      state.correctScreen = status
    }
  },
  getters: {
    allCompleted (state) {
      return state.sounds.every(sound => sound.selected === true)
    }
  }
})
