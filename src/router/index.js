import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Introduction from '@/components/Introduction'
import Stanza from '@/components/Stanza'
import AnimatedPoem from '@/components/AnimatedPoem'
import ExaminePoem from '@/components/ExaminePoem'
import SummaryView from '@/components/SummaryView'
import Credits from '@/components/Credits'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright,
      meta: {
        title: 'Copyright'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/stanza',
      name: 'Stanza',
      component: Stanza,
      meta: {
        title: 'Stanza'
      }
    },
    {
      path: '/animated-poem',
      name: 'AnimatedPoem',
      component: AnimatedPoem,
      meta: {
        title: 'Animated Poem'
      }
    },
    {
      path: '/examine-poem',
      name: 'ExaminePoem',
      component: ExaminePoem,
      meta: {
        title: 'Examine Poem'
      }
    },
    {
      path: '/summary',
      name: 'SummaryView',
      component: SummaryView,
      meta: {
        title: 'Summary'
      }
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits,
      meta: {
        title: 'Credits'
      }
    }
  ]
})
